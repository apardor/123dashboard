---
layout: post
title:  "Endre tekst nederst på nettsiden (footer)"
date:   2018-10-11 12:33:30 +0200

---

Gjelder endring av feltet som går igjen nederst på alle sider:

{% asset footer-1  %}


- Gå til **Kontrollpanel > MB Enfold > Footer.**

- Velg hvor mange kolonner footeren skal ha og om du ønsker å vise linker til sosiale medier.


- Gå til **Kontrollpanel > Utseende > Widgeter.**


- Trykk på "Footer - column". Finn f.eks. "Tekst" i listen over tilgjengelige widgeter og dra denne inn i riktig footerområde (obs: her må du skrive html-kode om du f.eks. ønsker linker eller fet skrift i teksten.)

{% asset footer-2 %}
