---
layout: post
title:  "Lag en portfolio"
date:   2018-10-11 12:33:30 +0200

---




 {% asset portfolio-1 %}

- Du kan få en portfolio tilsvarende den over om ønskelig.

- **Gå til kontrollpanel > Portfolio > Add new.**

- Trykk på **"Advanced Layout Editor"** om du ønsker å bygge en egen layout (anbefales).


{% asset portfolio-2  %}

- Bygg opp bredden på de ulike kolonnene ved å dra ned en og en kolonnebredde som vist på bildet under. Under har vi valgt å ha to like brede kolonner, 1/2.

{% asset portfolio-3 %}

- Når du har lagt inn kolonnene kan du starte å fylle disse med innhold. Gå til fanen "Content elements". Dra innholdselement du vil ha ned i en av kolonnene. Når kolonnen blir grønn indikerer det at elementet blir plassert der. Under har vi valgt å ha en tittel (special heading) øverst, en dekorativ linje (separator/Whitespace) under og et tekstfelt (text block). I kolonne nummer to har vi lagt til et galleri. Galleri og andre bildealternativer finner du under fanen "Media Elements".


{% asset portfolio-4 %}

- Trykk på de ulike innholdselementene for å tilpasse de ditt innhold.


- Deretter legger du til et fremhevet bilde (dette er bildet som vises på oversiktssiden for alle portfolioelementer). Du legger til et fremhevet bilde nede på siden, til høyre:

{% asset portfolio-5 %}

- Om ønskelig kan du legge til ulike portfoliokategorier. Det gjør du ved å trykke på "+ Legg til ny kategori" i kolonnen til høyre. Huk av for kategorien det nye portfolioelementet skal ligge i.
